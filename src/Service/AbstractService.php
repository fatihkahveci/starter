<?php
/**
 * Created by PhpStorm.
 * User: fatih
 * Date: 4.03.2019
 * Time: 16:09
 */

namespace App\Service;


use App\Entity\Article;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class AbstractService
{
    protected $entityManager;

    /**
     * AbstractService constructor.
     * @param $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }



    public function serializeArticleObject(Article $article)
    {
        return array(
            'id' => $article->getId(),
            'title' => $article->getTitle(),
            'description' => $article->getDescription(),
            'slug' => $article->getSlug(),
            'body' => $article->getBody(),
            'created_at' => $article->getCreatedAt(),
            'updated_at' => $article->getUpdatedAt(),
        );
    }

    public function serializeUserObject(User $user)
    {
        return array(
            'id' => $user->getId(),
            'username' => $user->getUsername(),
        );
    }


}