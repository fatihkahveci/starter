<?php
/**
 * Created by PhpStorm.
 * User: fatih
 * Date: 9.03.2019
 * Time: 13:41
 */

namespace App\Service;


use App\Entity\Tag;

class TagService extends AbstractService
{
    public function firstOrCreate($name)
    {
        $em = $this->getEntityManager();
        $tag = $em->getRepository(Tag::class)->findOneBy(array('name' => $name));

        if(is_null($tag)) {
            $tag = new Tag();
            $tag->setName($name);

            $em->persist($tag);
            $em->flush();
        }

        return $tag;
    }

    public function getAllTags()
    {
        $em = $this->getEntityManager();
        return $em->getRepository(Tag::class)->findAll();
    }
}