<?php
/**
 * Created by PhpStorm.
 * User: fatih
 * Date: 12.03.2019
 * Time: 15:31
 */

namespace App\Service;


use App\Entity\Follow;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService extends AbstractService
{
    protected $encoder;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder)
    {
        parent::__construct($entityManager);
        $this->encoder = $encoder;
    }

    public function update(User $user)
    {
        $em = $this->getEntityManager();
        $encodedPassword = $this->encoder->encodePassword($user, $user->getPassword());
        $user->setPassword($encodedPassword);


        $em->persist($user);
        $em->flush();


        return $user;
    }

    public function follow(User $follower , User $followed)
    {
        $em = $this->getEntityManager();

        $follow = new Follow();

        $follow->setFollower($follower);
        $follow->setFollowed($followed);

        $em->persist($follow);
        $em->flush();

        $followed->setFollowing(true);

        return $followed;
    }

    public function unFollow(User $follower , User $followed)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->addSelect('follow');
        $qb->delete(Follow::class, 'follow');
        $qb->where('follow.follower = :follower');
        $qb->andWhere('follow.followed = :followed');

        $qb->setParameters([
            'follower' => $follower,
            'followed' => $followed
        ]);

        return $qb->getQuery()->getResult();
    }

    public function getUserByEmail($email)
    {
        $em = $this->getEntityManager();

        return $em->getRepository(User::class)->findOneBy(array('email' => $email));
    }

    public function getUserByUsername($username)
    {
        $em = $this->getEntityManager();

        return $em->getRepository(User::class)->findOneBy(array('username' => $username));
    }

    public function checkCredentials($user,$password)
    {
        return $this->encoder->isPasswordValid($user, $password);
    }

}