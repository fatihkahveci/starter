<?php
/**
 * Created by PhpStorm.
 * User: fatih
 * Date: 4.03.2019
 * Time: 16:10
 */

namespace App\Service;


use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\Favorite;
use App\Entity\User;
use App\Utils\Slugger;
use Doctrine\ORM\EntityManagerInterface;

class ArticleService extends AbstractService
{
    protected $userService;
    protected $tagService;

    public function __construct(EntityManagerInterface $entityManager, UserService $userService, TagService $tagService)
    {
        parent::__construct($entityManager);
        $this->userService = $userService;
        $this->tagService = $tagService;
    }

    public function create(Article $article)
    {
        $em = $this->getEntityManager();

        $now = new \DateTime();
        $article->setCreatedAt($now);
        $article->setUpdatedAt($now);
        $article->setSlug(Slugger::slugify($article->getTitle()));

        $em->persist($article);
        $em->flush();

        return $article;
    }

    public function update(Article $article, User $user)
    {
        $em = $this->getEntityManager();

        $now = new \DateTime();
        $article->setUpdatedAt($now);
        $article->setSlug(Slugger::slugify($article->getTitle()));

        if($article->getUser()->getId() != $user->getId()) {
            return false;
        }

        $em->persist($article);
        $em->flush();

        return $article;
    }

    public function delete($slug, User $user)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->addSelect('article');
        $qb->delete(Article::class, 'article');
        $qb->where('article.slug = :slug');
        $qb->andWhere('article.user = :user');

        $qb->setParameters([
            ':slug' => $slug,
            ':user' => $user
        ]);

        return $qb->getQuery()->getResult();
    }

    public function attachTags(Article $article, $tags)
    {
        $em = $this->getEntityManager();

        foreach ($tags as $v) {
            $tag = $this->tagService->firstOrCreate($v);
            $article->addTag($tag);
        }
        $em->persist($article);
        $em->flush();

    }

    public function getBySlug($slug)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->addSelect('article');
        $qb->addSelect('user');
        $qb->addSelect('favorites');
        $qb->addSelect('tags');

        $qb->from(Article::class, 'article');
        $qb->leftJoin('article.user','user');
        $qb->leftJoin('article.tags','tags');
        $qb->leftJoin('user.favorites','favorites');

        $qb->where('article.slug = :slug');

        $qb->setParameter(':slug', $slug);

        return $qb->getQuery()->setMaxResults(1)->getOneOrNullResult();
    }

    public function getArticleFeed($limit = 20, $offset = 0, $tag = null, $favorited = null, $author = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->addSelect('article');
        $qb->addSelect('user');
        $qb->addSelect('tags');
        $qb->addSelect('favorites');

        $qb->from(Article::class, 'article');
        $qb->leftJoin('article.user','user');
        $qb->leftJoin('article.tags','tags');
        $qb->leftJoin('article.favorites','favorites');

        if($tag) {
            $qb->where('tags.name = :tag');
            $qb->setParameter(':tag', $tag);
        }

        if($favorited) {
            $qb->addSelect('favoritedUser');
            $qb->leftJoin('favorites.user', 'favoritedUser');
            $qb->andWhere('favoritedUser.username = :favorited');
            $qb->setParameter(':favorited', $favorited);
        }

        if($author) {
            $qb->andWhere('user.username = :author');
            $qb->setParameter(':author', $author);
        }

        $qb->orderBy('article.createdAt', 'desc');
        $qb->setFirstResult($offset);
        $qb->setMaxResults($limit);

        $query = $qb->getQuery();
        $paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($query);

        $articles = $query->getResult();

        if(is_null($articles)) {
            return null;
        }

        return [
            'articles' => $articles,
            'articlesCount' => count($paginator)
        ];
    }

    public function getArticleFeedByUser(User $user, $limit = 20, $offset = 0)
    {
        $followedIds = $user->getFollowingIds();

        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->addSelect('article');
        $qb->addSelect('user');
        $qb->addSelect('tags');
        $qb->addSelect('favorites');

        $qb->from(Article::class, 'article');
        $qb->where('article.user IN (:ids)');
        $qb->leftJoin('article.user','user');
        $qb->leftJoin('article.tags','tags');
        $qb->leftJoin('article.favorites','favorites');

        $qb->orderBy('article.createdAt', 'desc');
        $qb->setParameter(':ids' , $followedIds);
        $qb->setFirstResult($offset);
        $qb->setMaxResults($limit);

        $query = $qb->getQuery();
        $paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($query);

        $articles = $query->getResult();

        if(is_null($articles)) {
            return null;
        }

        return [
            'articles' => $articles,
            'articlesCount' => count($paginator)
        ];
    }

    public function getCommentsBySlug($slug,$limit = 20, $offset = 0)
    {
        $em = $this->getEntityManager();
        $article = $em->getRepository(Article::class)->findOneBy(['slug' => $slug]);

        if(is_null($article)) {
            return null;
        }

        $qb = $em->createQueryBuilder();
        $qb->addSelect('comment');
        $qb->addSelect('user');

        $qb->from(Comment::class, 'comment');
        $qb->where('comment.article = :id');
        $qb->leftJoin('comment.user','user');

        $qb->setParameter(':id' , $article->getId());
        $qb->setFirstResult($offset);
        $qb->setMaxResults($limit);

        $query = $qb->getQuery();
        $paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($query);
        $comments = $query->getResult();

        return [
            'comments' => $comments,
            'commentsCount' => count($paginator)
        ];
    }

    public function createComment($slug, Comment $comment)
    {
        $em = $this->getEntityManager();
        $date = new \DateTime();

        $article = $em->getRepository(Article::class)->findOneBy(['slug' => $slug]);

        if(is_null($article)) {
            return null;
        }

        $comment->setUpdatedAt($date);
        $comment->setCreatedAt($date);

        $em->persist($comment);
        $em->flush();

        return $comment;
    }

    public function favorite(Article $article, User $user)
    {
        $em = $this->getEntityManager();

        $favorite = new Favorite();
        $favorite->setArticle($article);
        $favorite->setUser($user);

        $em->persist($favorite);
        $em->flush();

        $article->setFavorited(true);

        return $article;
    }

    public function unFavorite(Article $article, User $user)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->addSelect('favorite');
        $qb->delete(Favorite::class, 'favorite');
        $qb->where('favorite.article = :article');
        $qb->andWhere('favorite.user = :user');

        $qb->setParameters([
            'article' => $article,
            'user' => $user
        ]);

        return $qb->getQuery()->getResult();
    }
}