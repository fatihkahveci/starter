<?php

namespace App\Controller\Api;

use App\Controller\AbstractApiController;

use App\Service\UserService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * @Route("api/profiles")
 */
class ProfileController extends AbstractApiController
{

    protected $userService;

    public function __construct(SerializerInterface $serializer, NormalizerInterface $normalizer, ValidatorInterface $validator, UserService $userService)
    {
        parent::__construct($serializer, $normalizer, $validator);
        $this->userService = $userService;
    }

    /**
     * @Route("/{username}", name="api_profiles_show", methods={"GET"})
     */
    public function show($username, Request $request)
    {
        $currentUser = $this->getUser();
        $userService = $this->userService;

        $user = $userService->getUserByUsername($username);

        if(is_null($user)) {
            return $this->notFoundResponse($username);
        }

        if($currentUser) {
            if(($userService->isFollowing($currentUser, $user))) {
                $user->setFollowing(true);
            }
        }

        return $this->profileResponse($user);
    }

    /**
     * @Route("/{username}/follow", name="api_profiles_follow", methods={"POST"})
     */
    public function follow($username, Request $request)
    {
        $currentUser = $this->getUser();
        $userService = $this->userService;

        $user = $userService->getUserByUsername($username);

        if(is_null($user)) {
            return $this->notFoundResponse($username);
        }

        $user = $userService->follow($currentUser,$user);

        return $this->profileResponse($user);
    }

    /**
     * @Route("/{username}/follow", name="api_profiles_unfollow", methods={"DELETE"})
     */
    public function unFollow($username, Request $request)
    {
        $currentUser = $this->getUser();
        $userService = $this->userService;

        $user = $userService->getUserByUsername($username);

        if(is_null($user)) {
            return $this->notFoundResponse($username);
        }

        $user = $userService->unFollow($currentUser,$user);

        return $this->profileResponse($user);
    }
}
