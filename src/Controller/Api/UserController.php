<?php

namespace App\Controller\Api;

use App\Controller\AbstractApiController;
use App\Entity\User;
use App\Form\UserType;
use App\Service\UserService;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * @Route("api")
 */
class UserController extends AbstractApiController
{

    protected $userService;

    public function __construct(SerializerInterface $serializer, NormalizerInterface $normalizer, ValidatorInterface $validator, UserService $userService)
    {
        parent::__construct($serializer, $normalizer, $validator);
        $this->userService = $userService;
    }


    /**
     * @Route("/users/login", name="api_user_login")
     */
    public function login(Request $request, JWTTokenManagerInterface $JWTManager)
    {
        $userService = $this->userService;
        $data = json_decode($request->getContent(),true)['user'];


        $user = $userService->getUserByEmail($data['email']);


        if(!$userService->checkCredentials($user,$data['password'])) {
            exit("olmadi");
        }

        $userResponse = $this->normalizer->normalize($user,null, array('groups' => 'userDetail'));
        $userResponse['token'] = $JWTManager->create($user);
        $userResponse['password'] = $data['password'];

        return $this->json($userResponse);
    }

    /**
     * @Route("/users", name="api_users_create", methods={"POST"})
     */
    public function create(Request $request, JWTTokenManagerInterface $JWTManager)
    {
        $userService = $this->userService;
        $data = json_decode($request->getContent(),true)['user'];
        $user = new User();


        $form = $this->createForm(UserType::class, $user);
        $form->submit($data);
        $errors = $this->validator->validate($user);


        if(count($errors) >= 1) {
            return $this->errorResponse($errors);
        }

        $user = $userService->update($user);

        $userResponse = $this->normalizer->normalize($user,null, array('groups' => 'userDetail'));
        $userResponse['token'] = $JWTManager->create($user);
        $userResponse['password'] = $data['password'];

        return $this->json($userResponse);
    }


    /**
     * @Route("/user", name="api_users_show", methods={"GET"})
     */
    public function show(Request $request)
    {
        $user = $this->getUser();

        return $this->json(['user' => $this->normalizer->normalize($user, null, ['groups' => 'userDetail'])]);
    }


    /**
     * @Route("/user", name="api_users_update", methods={"PUT"})
     */
    public function update(Request $request)
    {
        $userService = $this->userService;
        $data = json_decode($request->getContent(),true)['user'];
        $user = $this->getUser();

        $form = $this->createForm(UserType::class, $user);
        $form->submit($data, false);
        $errors = $this->validator->validate($user);


        if(count($errors) >= 1) {
            return $this->errorResponse($errors);
        }

        $userService->update($user);

        return $this->json(['user' => $this->normalizer->normalize($user, null, ['groups' => 'userDetail'])]);
    }
}
