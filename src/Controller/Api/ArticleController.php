<?php

namespace App\Controller\Api;

use App\Controller\AbstractApiController;
use App\Entity\Article;
use App\Entity\Comment;
use App\Form\ArticleType;
use App\Form\CommentType;
use App\Service\ArticleService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("api/articles")
 */
class ArticleController extends AbstractApiController
{

    protected $articleService;

    public function __construct(SerializerInterface $serializer, NormalizerInterface $normalizer, ValidatorInterface $validator, ArticleService $articleService)
    {
        parent::__construct($serializer, $normalizer, $validator);
        $this->articleService = $articleService;
    }

    /**
     * @Route("", name="api_articles_create", methods={"POST"})
     */
    public function create(Request $request)
    {
        if(!$this->getUser()) {
            return $this->unauthorizedResponse();
        }

        $articleService = $this->articleService;
        $data = json_decode($request->getContent(), true)['article'];
        $article = new Article();


        $form = $this->createForm(ArticleType::class, $article);
        $form->submit($data);
        $errors = $this->validator->validate($article);

        if(count($errors) >= 1) {
            return $this->errorResponse($errors);
        }

        $article->setUser($this->getUser());
        $article = $articleService->create($article);

        if($data['tagList']) {
            $articleService->attachTags($article,$data['tagList']);
        }

        return $this->articleResponse($article);
    }

    /**
     * @Route("/{slug}", name="api_articles_delete", methods={"DELETE"})
     */
    public function delete($slug, Request $request)
    {
        if(is_null($this->getUser())) {
            return $this->unauthorizedResponse();
        }

        $articleService = $this->articleService;
        $articleService->delete($slug, $this->getUser());

        return $this->statusResponse(true);
    }

    /**
     * @Route("/{slug}", name="api_articles_update", methods={"PUT"})
     */
    public function update($slug, Request $request)
    {
        if(is_null($this->getUser())) {
            return $this->unauthorizedResponse();
        }

        $articleService = $this->articleService;
        $data = json_decode($request->getContent(), true)['article'];

        $article = $articleService->getBySlug($slug);

        $form = $this->createForm(ArticleType::class, $article);
        $form->submit($data, false);

        $errors = $this->validator->validate($article);

        if(count($errors) >= 1) {
            return $this->errorResponse($errors);
        }

        $article = $articleService->update($article, $this->getUser());

        if(!$article) {
            return $this->unauthorizedResponse();
        }

        return $this->articleResponse($article);
    }

    /**
     * @Route("", name="api_articles_index", methods={"GET"})
     */
    public function index(Request $request)
    {
        $tag = $request->get('tag');
        $author = $request->get('author');
        $favorited = $request->get('favorited');
        $limit = $request->get('limit', 20);
        $offset = $request->get('offset',0);
        $articleService = $this->articleService;

        return $this->multipleArticleResponse($articleService->getArticleFeed($limit,$offset,$tag,$favorited,$author));
    }

    /**
     * @Route("/feed", name="api_articles_feed", methods={"GET"})
     */
    public function feed(Request $request)
    {
        $limit = $request->get('limit', 20);
        $offset = $request->get('offset',0);

        $articles = $this->articleService->getArticleFeedByUser($this->getUser(), $limit, $offset);

        return $this->multipleArticleResponse($articles);
    }

    /**
     * @Route("/{slug}", name="api_articles_show", methods={"GET"})
     */
    public function show($slug)
    {
        $articleService = $this->articleService;
        $article = $articleService->getBySlug($slug);

        if(is_null($article)) {
            return $this->notFoundResponse($slug);
        }

        return $this->articleResponse($article);
    }

    /**
     * @Route("/{slug}/favorite", name="api_articles_favorite", methods={"POST"})
     */
    public function favorite($slug)
    {
        $articleService = $this->articleService;
        $article = $articleService->getBySlug($slug);

        if(is_null($article)) {
            return $this->notFoundResponse($slug);
        }

        return $this->articleResponse($articleService->favorite($article, $this->getUser()));
    }

    /**
     * @Route("/{slug}/favorite", name="api_articles_unfavorite", methods={"DELETE"})
     */
    public function unFavorite($slug)
    {
        $articleService = $this->articleService;
        $article = $articleService->getBySlug($slug);

        if(is_null($article)) {
            return $this->notFoundResponse($slug);
        }

        return $this->articleResponse($article);
    }

    /**
     * @Route("/{slug}/comments", name="api_articles_comments", methods={"GET"})
     */
    public function comments($slug, Request $request)
    {
        $limit = $request->get('limit', 20);
        $offset = $request->get('offset',0);

        $articleService = $this->articleService;
        $comments = $articleService->getCommentsBySlug($slug,$limit,$offset);

        if(is_null($comments)) {
            return $this->notFoundResponse($slug);
        }

        return $this->commentsResponse($comments);
    }

    /**
     * @Route("/{slug}/comments", name="api_articles_comments_create", methods={"POST"})
     */
    public function createComment($slug, Request $request)
    {
        if(is_null($this->getUser())) {
            return $this->unauthorizedResponse();
        }

        $articleService = $this->articleService;
        $data = json_decode($request->getContent(), true)['comment'];
        $comment = new Comment();


        $form = $this->createForm(CommentType::class, $comment);
        $form->submit($data);
        $errors = $this->validator->validate($comment);

        if(count($errors) >= 1) {
            return $this->errorResponse($errors);
        }

        $comment->setUser($this->getUser());
        $comment = $articleService->createComment($slug,$comment);

        return $this->commentResponse($comment);
    }

}
