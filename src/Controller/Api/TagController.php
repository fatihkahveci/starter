<?php

namespace App\Controller\Api;

use App\Controller\AbstractApiController;
use App\Entity\Tag;
use App\Form\TagType;
use App\Service\TagService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("api/tags")
 */
class TagController extends AbstractApiController
{

    protected $tagService;

    public function __construct(SerializerInterface $serializer, NormalizerInterface $normalizer, ValidatorInterface $validator, TagService $tagService)
    {
        parent::__construct($serializer, $normalizer, $validator);
        $this->tagService = $tagService;
    }

    /**
     * @Route("/", name="api_tags_index", methods={"GET"})
     */
    public function index(Request $request)
    {
        $tagService = $this->tagService;
        return $this->tagsResponse($tagService->getAllTags());
    }
}
