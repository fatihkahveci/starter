<?php
/**
 * Created by PhpStorm.
 * User: fatih
 * Date: 4.03.2019
 * Time: 16:05
 */


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class AbstractApiController extends AbstractController
{
    protected $serializer;
    protected $normalizer;
    protected $validator;

    public function __construct(SerializerInterface $serializer, NormalizerInterface $normalizer, ValidatorInterface $validator)
    {
        $this->serializer = $serializer;
        $this->normalizer = $normalizer;
        $this->validator = $validator;
    }

    protected function statusResponse($status)
    {
        return $this->json(['success' => $status]);
    }

    protected function errorResponse($errors)
    {
        $response = [
            'errors' => []
        ];
        foreach ($errors as $error) {
            $response['errors'][$error->getPropertyPath()] = $error->getMessage();
        }

        return $this->json($response, 422);
    }


    protected function unauthorizedResponse()
    {
        return $this->json([
            'errors' => [
                'message' => 'unauthorized'
            ]
        ],401);
    }

    protected function notFoundResponse($search)
    {
        return $this->json([
            'errors' => [
                'message' => $search . ' not found',
            ]
        ],404);
    }

    protected function articleResponse($article)
    {
        $user = $this->getUser();

        if($user) {
            $followedIds = $user->getFollowingIds();
            $favoriteIds = $user->getFavoritesIds();


            if(in_array($article->getUser()->getId(), $followedIds)) {
                $article->getUser()->setFollowing(true);
            }

            if(in_array($article->getId(), $favoriteIds)) {
                $article->setFavorited(true);
            }
        }

        return $this->json([
            'article' => $this->normalizer->normalize($article, null, ['groups' => 'articleDetail'])
        ]);
    }

    protected function multipleArticleResponse($articles)
    {
        $user = $this->getUser();

        if($user) {
            $favoriteIds = $user->getFavoritesIds();
            foreach ($articles['articles'] as $article) {
                $article->getUser()->setFollowing(true);
                if(in_array($article->getId(), $favoriteIds)) {
                    $article->setFavorited(true);
                }
            }
        }

        return $this->json($this->normalizer->normalize($articles,null, ['groups' => 'articleDetail']));
    }

    protected function userResponse($user)
    {

    }

    protected function profileResponse($user)
    {
        return $this->json(['profile' => $this->normalizer->normalize($user,null, ['groups' => 'profileDetail'])]);
    }

    protected function commentResponse($comment)
    {
        return $this->json([
            'comment' => $this->normalizer->normalize($comment, null, ['groups' => 'commentDetail'])
        ]);
    }

    protected function multipleCommentResponse($comments)
    {
        $user = $this->getUser();

        if($user) {
            $followedIds = $user->getFollowingIds();

            foreach ($comments['comments'] as $comment) {
                if(in_array($comment->getUser()->getId(), $followedIds)) {
                    $comment->getUser()->setFollowing(true);
                }
            }
        }

        return $this->json($this->normalizer->normalize($comments,null, ['groups' => 'commentDetail']));
    }

    protected function tagsResponse($tags)
    {
        return $this->json(['tags' => $this->normalizer->normalize($tags, null, ['groups' => 'tagDetail'])]);
    }
}