<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\Favorite;
use App\Entity\Follow;
use App\Entity\Tag;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    protected $encoder;
    protected $faker;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
        $this->faker = \Faker\Factory::create();

    }

    public function load(ObjectManager $manager)
    {
        $encoder = $this->encoder;
        $generator = \Faker\Factory::create();
        $populator = new \Faker\ORM\Doctrine\Populator($generator, $manager);
        $populator->addEntity(User::class, 5, [
            'password' => function() use($generator,$encoder) {
                //return $generator->password;
                return 'password';
            },
            'image' => function() use($generator) {
                return $generator->imageUrl(640,480, 'people');
            }
        ], [
            function($user) use($encoder) { $user->setPassword($encoder->encodePassword($user, $user->getPassword())); },

        ]);

        $populator->addEntity(Article::class, 250, [
            'slug' => function() use($generator) {
                return $generator->slug;
            },
        ]);
        $populator->addEntity(Comment::class, 250);
        $populator->addEntity(Follow::class, 100);
        $populator->addEntity(Favorite::class, 100);

        $insertedPKs = $populator->execute();

        /*$articles = $insertedPKs["App\Entity\Article"];
        $tags = $insertedPKs["App\Entity\Tag"];


        foreach ($articles as $article) {

            $article->addTag($tags[0]);
            $manager->persist($article);
        }

        $manager->flush();*/

    }
}
