<?php

use PHPUnit\Framework\TestCase;

class SluggerTest extends TestCase
{
    public function testSlugify()
    {
        $slug =  \App\Utils\Slugger::slugify('test slugify');

        $this->assertEquals('test-slugify', $slug);

    }
}