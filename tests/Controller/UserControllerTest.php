<?php

namespace App\Tests\Controller;


use App\Tests\RealWorldTestCase;

class UserControllerTest extends RealWorldTestCase
{
    public function testCreate()
    {
        $client = static::createClient();


        $userData = [
            'user' => [
                'username' => 'admin',
                'password' => 'admin',
                'email' => 'admin@mail.com',
            ]
        ];

        $client->request(
            'POST',
            '/api/users',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($userData)
        );


        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testLogin()
    {
        $client = static::createClient();


        $userData = [
            'user' => [
                'password' => 'admin',
                'email' => 'admin@mail.com',
            ]
        ];

        $client->request(
            'POST',
            '/api/users/login',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($userData)
        );


        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}